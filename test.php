<?php 
header('Content-Type: text/html; charset=utf-8');
require('classes/integra.class.php');

$URL = 'http://localhost:8000/';
// $URL = 'http://integra.telessaude.ufrn.br/';

# Criando e alimentando os quadros
$quadroUm = new QuadroUm(100, 1000, 500);
$quadroUm->addAvaliacaoEstrutura(240810,10,5,5);
$quadroUm->addProfissionaisRegistrados(240810, 2231, 50);
$quadroUm->addProfissionaisRegistrados(240810, 2235, 70);

$quadroDois = new QuadroDois(100, 1, 50, 1);
$quadroDois->addSolicitacoesUF(24, 500);
$quadroDois->addSolicitacoesMunicipio(240810, 401);
$quadroDois->addSolicitacoesEquipe(1, 100);
$quadroDois->addSolicitacoesPontoTelessaude(2653982, 400);
$quadroDois->addSolicitacoesCatProfissional(2235, 100);
$quadroDois->addSolicitacoesMembroGestao(1, 'Fulano da Silva Sauro', 30);
$quadroDois->addSolicitacoesProfissional(1, 'Siclano da Silva Sauro', 96);

$quadroTres = new QuadroTres(10, 99);
$quadroTres->addSolicitacoesTelediagnosticoUF(24, 500);
$quadroTres->addSolicitacoesTelediagnosticoMunicipio(240810, 10);
$quadroTres->addSolicitacoesTelediagnosticoEquipe(1, 5);
$quadroTres->addSolicitacoesTelediagnosticoPontoTelessaude(2653982, 100);
$quadroTres->addSolicitacoesTelediagnosticoTipo(1, 10);

$quadroQuatro = new QuadroQuatro(50, 1);
$quadroQuatro->addAtividadesRealizadasUF(24, 10);
$quadroQuatro->addAtividadesRealizadasMunicipio(240810, 10);
$quadroQuatro->addAtividadesRealizadasPontoTelessaude(2653982, 10);
$quadroQuatro->addAtividadesRealizadasEquipe(1, 10);
$quadroQuatro->addParticipantesCatProfissionalUF(24, 2251, 25);
$quadroQuatro->addParticipantesCatProfissionalMunicipio(240810, 2251, 25);
$quadroQuatro->addParticipantesCatProfissionalEquipe(1, 2251, 10);
$quadroQuatro->addParticipantesCatProfissionalPontoTelessaude(2653982, 2251, 10);
$quadroQuatro->addAcessosObjetosAprendizagem(24, 240810, 1, 2653982, 10);
$quadroQuatro->addAcessosObjetosAprendizagemMunicipio(240810, 10);
$quadroQuatro->addAcessosObjetosAprendizagemEquipe(1, 10);
$quadroQuatro->addAcessosObjetosAprendizagemPontoTelessaude(2653982, 10);
$quadroQuatro->addAcessosObjetosAprendizagemCatProfissional(2251, 10);

$quadroCinco = new QuadroCinco(50, 5, 25, 0, 70);
$quadroCinco->AddTemasFrequentes('R05', 'R05');
$quadroCinco->AddTemasFrequentes('R50', 'A03');
$quadroCinco->AddTemasFrequentes('R070', 'R21');
$quadroCinco->AddTemasFrequentes('R11', 'D10');
$quadroCinco->AddTemasFrequentes('K591', 'D11');
$quadroCinco->AddEspecialidadesFrequentes(225125);
$quadroCinco->AddEspecialidadesFrequentes(225135);
$quadroCinco->AddEspecialidadesFrequentes(322205);
$quadroCinco->AddEspecialidadesFrequentes(223565);
$quadroCinco->AddCatProfissionaisFrequentes(2231);
$quadroCinco->AddCatProfissionaisFrequentes(3222);
$quadroCinco->AddCatProfissionaisFrequentes(2235);
$quadroCinco->AddSatisfacaoSolicitante(1, 76);
$quadroCinco->AddSatisfacaoSolicitante(2, 21);
$quadroCinco->AddSatisfacaoSolicitante(3, 3);
$quadroCinco->setResolucaoDuvida(75, 20, 5, 5);

$quadroSeis = new QuadroSeis();
$quadroSeis->AddTemasFrequentesParticipacao(1);
$quadroSeis->AddTemasFrequentesParticipacao(2);
$quadroSeis->AddTemasFrequentesObjetoAprendizagem(1);
$quadroSeis->AddTemasFrequentesObjetoAprendizagem(2);
$quadroSeis->AddAvaliacaoSatisfacaoParticipantes(1, 70);
$quadroSeis->AddAvaliacaoSatisfacaoParticipantes(2, 25);
$quadroSeis->AddAvaliacaoSatisfacaoParticipantes(3, 5);
$quadroSeis->AddAvaliacaoSatisfacaoObjetoAprendizagem(1, 70);
$quadroSeis->AddAvaliacaoSatisfacaoObjetoAprendizagem(2, 22);
$quadroSeis->AddAvaliacaoSatisfacaoObjetoAprendizagem(3, 8);

# Agrupando os quadros de indicadores 
$indicadorGeralN1 = new IndicadorGeral(1, "042014", $quadroUm, $quadroDois, $quadroTres, $quadroQuatro, $quadroCinco, $quadroSeis);
$indicadorGeralN2 = new IndicadorGeral(1, "052014", null, null, $quadroTres, $quadroQuatro, $quadroCinco, null);
$indicadorGeralN3 = new IndicadorGeral(1, "062014", $quadroUm, null, $quadroTres, null, $quadroCinco, null);
$indicadorGeralN4 = new IndicadorGeral(1, "072014", null, $quadroDois, null, $quadroQuatro, null, $quadroSeis);

# Serializando os dados
$dados_serializadosN1 = Integra::serializar(TipoDeDados::JSON, $indicadorGeralN1);
$dados_serializadosN2 = Integra::serializar(TipoDeDados::JSON, $indicadorGeralN2);
$dados_serializadosN3 = Integra::serializar(TipoDeDados::JSON, $indicadorGeralN3);
$dados_serializadosN4 = Integra::serializar(TipoDeDados::JSON, $indicadorGeralN4);

echo '<h1>.: Dados Serializados :.</h1>';
echo $dados_serializadosN1;
echo '<hr/>';
echo $dados_serializadosN2;
echo '<hr/>';
echo $dados_serializadosN3;
echo '<hr/>';
echo $dados_serializadosN4;
echo '<hr/>';

$integra = new Integra('dd5706e91b1a37826a802ce272e0342d97f4afec');
// $integra = new Integra('5075669377e2fa89b5f88e0abf1518f99381d6da');
$respostaN1 = $integra->enviarDados(TipoDeDados::JSON, $URL.'api/indicadores/.json', $dados_serializadosN1);
$respostaN2 = $integra->enviarDados(TipoDeDados::JSON, $URL.'api/indicadores/.json', $dados_serializadosN2);
$respostaN3 = $integra->enviarDados(TipoDeDados::JSON, $URL.'api/indicadores/.json', $dados_serializadosN3);
$respostaN4 = $integra->enviarDados(TipoDeDados::JSON, $URL.'api/indicadores/.json', $dados_serializadosN4);

echo '<h1>.: Resposta do Servidor :.</h1>';
echo $respostaN1;
echo '<hr/>';
echo $respostaN2;
echo '<hr/>';
echo $respostaN3;
echo '<hr/>';
echo $respostaN4;
echo '<hr/>';

?>